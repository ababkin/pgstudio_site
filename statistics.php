<?php 
	include("usefull.php");
	include("connect.php");
	include("parts.php");

	$user_data = null;
	$statistics_data = null;
	
	$date_range = $_POST["date_range"];
	$app_selector = $_POST["app_selector"];
	
	$sessid = isset($_COOKIE["save_sessid"]) ? $_COOKIE["save_sessid"] : "";
	
	if ($sessid!=""){
		try {
			$DBH = new PDO("mysql:host=$db_host;dbname=$db_name", $db_user, $db_pass);
			$DBH->exec('USE '.$db_name.';');	
			$DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
			
			$STH = $DBH->prepare("SET NAMES 'utf8';SET CHARACTER SET 'utf8';SET SESSION collation_connection = 'utf8_general_ci';");
			$STH->execute();
			
			$STH = $DBH->prepare("SELECT * FROM users WHERE session=?");
			$STH->execute(array($sessid));
			$data_obj = $STH->fetch();
			
			if ($data_obj){
				$user_data = $data_obj;
				
				//parse app data
				$STH = $DBH->prepare("SELECT * FROM apps WHERE user=? ORDER BY id DESC");
				$STH->execute(array($user_data["id"]));
				$apps_data = $STH->fetchAll();
				
				//update statistics
				if (isset($date_range) && $date_range!="" && isset($app_selector) && $app_selector!=""){
					$date_from = explode(" - ", $date_range)[0];
					$date_to = explode(" - ", $date_range)[1];
					
					
					if ($app_selector=="all"){
						$STH = $DBH->prepare("SELECT * FROM statistics WHERE user=? AND date >= ? AND date <= ? ORDER BY date ASC");
						$STH->execute(array($user_data["id"],$date_from,$date_to));
					}else{
						$STH = $DBH->prepare("SELECT * FROM statistics WHERE user=? AND app=? AND date >= ? AND date <= ? ORDER BY date ASC");
						$STH->execute(array($user_data["id"], $app_selector,$date_from,$date_to));
					}
					
					$statistics_data = $STH->fetchAll();
				}
				
			}else{
				localRedirect("/");
			}
				

		}catch(PDOException $e) {  
			echo($e->getMessage());
		}
	}else{
		localRedirect("/");
	}
?>

<?php echo getHeader($user_data["firstname"],$user_data["balance"], "statistics"); ?>
<div class="content-section">
	<div class="cs-head">
		<h2>СТАТИСТИКА</h2>
	</div>
	<div class="cs-body white-bg">
		<div class="cs-left-full clearfix">
			<div class="left-form">
				<form class="blue-form" method='post' id="form">
					<label>
						Период 
						<input type="text" class="select-date-range" id="date_range" name="date_range" value="<?php echo $date_range;?>" placeholder="2017-01-22 - 2017-01-25"/>
						<a id="this_day" onclick="form.submit();" href="#"><small>За этот день</small></a><br>
						<a id="this_month" onclick="form.submit();" href="#"><small>За этот месяц</small></a><br>
						<br>
						
					</label>
					<label>
						Приложение
						<select name="app_selector">
							<option value="all">Показать по всем</option>
							<?php
								for ($i=0;$i<count($apps_data);$i++){
									$name = $apps_data[$i]["name"];
									$id = $apps_data[$i]["id"];
									$selected = ($app_selector==$id)? "selected" : "";
									echo "<option $selected value='$id'>$name</option>";
								}
							?>
						</select>
					</label>
					<button type="submit" id="magic_btn" class="btn btn-blue-square btn-stats btn-fullwidth">Посмотреть статистику</button>
				</form>
				<script>
				
			</script>
			</div>
			<div class="cs-right-table">
				<table class="striped-table">
					<tr>
						<th>Дата</th>
						<th>Кол-во установок</th>
					</tr>
					<?php 
						
						$new_statistics = array();
						
						$installs_sum = 0;
						
						for ($i=0;$i<count($statistics_data);$i++){
							$date = $statistics_data[$i]["date"];
							$installs = $statistics_data[$i]["installs"];
							
							$installs_sum+=$installs;
							
							$last_date = (count($new_statistics)>0) ? $new_statistics[count($new_statistics)-1]["date"] : null;
							if ($date!=$last_date){
								$obj = array();
								$obj["date"] = $date;
								$obj["installs"] = $installs;
								$new_statistics[] = $obj;
							}else{
								$new_statistics[count($new_statistics)-1]["installs"] += $installs;
							}
								
						}
						
						for ($i=0;$i<count($new_statistics);$i++){
							$date = $new_statistics[$i]["date"];
							$installs = $new_statistics[$i]["installs"];
							echo getStatisticsPart($date, $installs);
						}
						
					?>
					<tr class="total">
						<td>ИТОГО</td>
						<td><?php echo $installs_sum;?></td>
					</tr>
				</table>
			</div>
		</div>
		
		<script>
			
		function getCorrectNum(num){
			if (num>9) return ""+num;
			else return "0"+num;
		}
	$(document).ready(function() {
		$('#this_day').click(function () {
			var d = new Date();
			var curr_date = getCorrectNum(d.getDate());
			var curr_month = getCorrectNum(d.getMonth()+1);
			var curr_year = getCorrectNum(d.getFullYear());
			$("#date_range").val(curr_year+"-"+curr_month+"-"+curr_date+" - "+curr_year+"-"+curr_month+"-"+curr_date);
			 //alert("ek")
			 $( "#magic_btn" ).trigger( "click" );

		});
		
		$('#this_month').click(function () {
			var d = new Date();
			var curr_date = getCorrectNum(d.getDate());
			var curr_month = getCorrectNum(d.getMonth()+1);
			var curr_year = getCorrectNum(d.getFullYear());
			$("#date_range").val(curr_year+"-"+curr_month+"-0"+1+" - "+curr_year+"-"+curr_month+"-"+curr_date);
			 //alert("ek")
			 $( "#magic_btn" ).trigger( "click" );

		});
				});			
	</script>
	
	<script>
		$(document).ready(function() {
			
			$( "form" ).submit(function( event ) {
				$.cookie("scroll", $(document).scrollTop() );
			});

			// If cookie is set, scroll to the position saved in the cookie.
			if ( $.cookie("scroll") !== null ) {
				$(document).scrollTop( $.cookie("scroll") );
			}

			// When a button is clicked...
			$('#submit').on("click", function() {

				// Set a cookie that holds the scroll position.
				$.cookie("scroll", $(document).scrollTop() );

			});
			
			var d = new Date();
			var curr_date = getCorrectNum(d.getDate());
			var curr_month = getCorrectNum(d.getMonth()+1);
			var curr_year = getCorrectNum(d.getFullYear());
			
			if ($("#date_range").val()=="")
				$("#date_range").val(curr_year+"-"+curr_month+"-"+curr_date+" - "+curr_year+"-"+curr_month+"-"+curr_date);

		});
	</script>
		
	</div>
</div>

<?php echo getFooter(); ?>