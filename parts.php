<?php
function getFooter() {
	return "</div><!--/container-->
		</div><!--/page-bg-->

		<footer class='site-footer'>
			<div class='footer-top'>
				<div class='container'>
					<div class='ft-left'>
						<a href='javascript:location.reload(true)'><img src='img/logo_footer.png' alt='' /></a>
					</div>
					<div class='ft-right'>
						<button class='btn btn-white'><i class='i-call'></i>Перезвоните мне</button>
					</div>
				</div>
			</div>

			<div class='footer-bottom'>
				<div class='container'>
					<nav class='footer-nav'>
						<ul>
							<li><a href='/cabinet.php'>Главная</a></li>
							<li><a href='/statistics.php'>Статистика</a></li>
							<li><a href='/balance.php'>Пополнить баланс</a></li>
							<li><a href='/balance.php#history'>История операций</a></li>
							<li><a href='/partner.php'>Партнерская программа</a></li>
						</ul>
					</nav>

					<div class='copyright'>
						<p>&copy; 2016. All rights reserved.</p>
					</div>
				</div>
			</div>
		</footer>

	</div><!--/full-page-->

<!-- /Google Analytics -->
<!-- script>
	(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
	(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
	m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
	})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
	
	ga('create', 'UA-63938217-2', 'auto');
	ga('send', 'pageview');
	
</script -->
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-91013877-1', 'auto');
  ga('send', 'pageview');

</script>
<!-- /Google Analytics -->

<!-- Код тега ремаркетинга Google -->
<!---->
<script type='text/javascript'>
	/* <![CDATA[ */
	var google_conversion_id = 945538711;
	var google_custom_params = window.google_tag_params;
	var google_remarketing_only = true;
	/* ]]> */
</script>
<script type='text/javascript'
	src='//www.googleadservices.com/pagead/conversion.js'>
</script>
<noscript>
	<div style='display: inline;'>
		<img height='1' width='1' style='border-style: none;' alt=''
			src='//googleads.g.doubleclick.net/pagead/viewthroughconversion/945538711/?value=0&amp;guid=ON&amp;script=0' />
	</div>
</noscript>
<script type='text/javascript' src='//consultsystems.ru/script/27499/'
	charset='utf-8' async></script>
</body>
</html>";
}
function getHeader($firstname, $balance, $page_id) {
	$pages = array ();
	$pages ["cabinet"] = "";
	$pages ["statistics"] = "";
	$pages ["balance"] = "";
	$pages ["history"] = "";
	$pages ["partner"] = "";
	
	$pages [$page_id] = "class='active'";
	
	return "<!DOCTYPE html>
<html>
<head>
	<meta charset='UTF-8'>
	<meta http-equiv='content-type' content='text/html; charset=utf-8'/>
	<meta name='viewport' content='width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no'>
	<title>Rocket4Apps</title>
	<link rel='stylesheet' href='css/style.css'>
	<link rel='stylesheet' href='css/daterangepicker.min.css'>
	<script src='https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js'></script>
	<script src='js/velocity.min.js'></script>
	<script src='js/jquery.cookie.js'></script>
	<script src='js/jquery.tabslet.min.js'></script>
	<script src='js/jquery.basictable.min.js'></script>
	<script src='js/moment.min.js'></script>
	<script src='js/jquery.daterangepicker.min.js'></script>
	<script src='js/functions.js'></script>
</head>
<body>

	<div id='full-page'>

		<header class='site-header'>
			<div class='header-line-1'>
				<div class='container'>
					<div class='hl1-item'>
						<span class='head-greating'>Здравствуйте, $firstname</span>
						<a href='/' class='head-logout'>Выход</a>
					</div>
					<div class='hl1-item'>
						<span class='head-balance'><a href='/balance.php'>Ваш баланс: $balance <strong>USD</strong></a></span>
					</div>
					<div class='hl1-item'>
						<span class='head-callme'>Перезвоните мне</span>
					</div>
				</div>
			</div>

			<div class='header-line-2'>
				<div class='container'>
					<div class='header-logo'>
						<div class='lang-menu'>
							<ul>
								<li><a href='https://my.rocket4app.com'>en</a></li>
								<li class='active'><a>ru</a></li>
							</ul>
						</div>
						<a href='javascript:location.reload(true)'>
							<img src='img/logo.png' alt='' />
						</a>
					</div>
					<nav class='header-menu'>
						<ul>
							<li ${pages["cabinet"]}><a href='/cabinet.php'>КАБИНЕТ</a></li>
							<li ${pages["statistics"]}><a href='/statistics.php'>СТАТИСТИКА</a></li>
							<li ${pages["balance"]}><a href='/balance.php'>ПОПОЛНИТЬ БАЛАНС</a></li>
							<li ${pages["history"]}><a href='/balance.php#history'>ИСТОРИЯ ОПЕРАЦИЙ</a></li>
							<li ${pages["partner"]}><a href='/partner.php'>ПАРТНЕРСКАЯ ПРОГРАММА</a></li>
						</ul>
					</nav>
					<div class='toggle-menu'>
						<span></span>
					</div>
					<div class='mobile-menu'>
						<div class='hl1-item'>
							<span class='head-greating'>Здравствуйте, $firstname!</span>
							<a href='/' class='head-logout'>Выход</a>
						</div>
						<div class='hl1-item'>
							<span class='head-balance'>Ваш баланс: $balance <strong>USD</strong></span>
						</div>
						<div class='hl1-item'>
							<span class='head-callme'>Перезвоните мне</span>
						</div>

						<nav>
							<ul>
								<li ${pages["cabinet"]}><a href='/cabinet.php'>КАБИНЕТ</a></li>
								<li ${pages["statistics"]}><a href='/statistics.php'>СТАТИСТИКА</a></li>
								<li ${pages["balance"]}><a href='/balance.php'>ПОПОЛНИТЬ БАЛАНС</a></li>
								<li ${pages["history"]}><a href='/balance.php#history'>ИСТОРИЯ ОПЕРАЦИЙ</a></li>
								<li ${pages["partner"]}><a href='/partner.php'>ПАРТНЕРСКАЯ ПРОГРАММА</a></li>
							</ul>
						</nav>
					</div>
				</div>
			</div>
		</header>

		<div class='page-bg'>
			<div class='container'>";
}
function getTransactionsPart($date, $value, $msg, $status) {
	$year = explode ( "-", $date )[0];
	$month = explode ( "-", $date )[1];
	$day = explode ( "-", $date )[2];
	
	echo "<tr>
				<td>$year / $month / $day</td>
				<td class='referral-earned'>$value USD</td>
				<td>$msg</td>
				<td>$status</td>
			</tr>";
}
function getReferalPart($email, $earned, $date) {
	$year = explode ( "-", $date )[0];
	$month = explode ( "-", $date )[1];
	$day = explode ( "-", $date )[2];
	
	echo "<tr>
				<td>$email</td>
				<td class='referral-earned'>$earned USD</td>
				<td>$year / $month / $day</td>
			</tr>";
}
function getStatisticsPart($date, $installs) {
	echo "<tr>
					<td>$date</td>
					<td>$installs</td>
				 </tr>";
}
function getCompanyPart($status, $appName, $companyName, $countriesArr, $keywords, $hourLimit, $dayLimit, $monthLimit, $todayInstalls, $price, $totalInstalls, $hidden, $id, $camp_id, $notfound) {
	$checkedText = ($status) ? "checked='checked'" : "";
	$countries = "";
	
	for($i = 0; $i < count ( $countriesArr ); $i ++) {
		$countries = $countries . "<option name='$countriesArr[$i]'>$countriesArr[$i]</option>\n";
	}
	
	$hiddenText = ($hidden) ? "checked='checked'" : "";
	
	$on_msg = 'Кампания работает, выключить?';
	$off_msg = 'Кампания остановлена, включить?';
	$onoff_tag = ($status) ? "data-tooltip='$on_msg' style='color: #FF0000'" : "data-tooltip='$off_msg' style='color: #FF0000'";
	
	$help_msg = 'Приложение не найдено в поиске по запросу "' . $keywords . '"';
	$td_tag = ($notfound < 5) ? "<td>" : "<td data-tooltip='$help_msg' style='color: #FF0000'>";
	
	return "<tr>
					<td $onoff_tag>
						<label class='switch'>
						  <input type='checkbox' $checkedText name='running_$id'/>
						  <div class='slider'></div>
						</label>
					</td>
					<input type='hidden' name='id_$id' value='$camp_id'>
					<td>
						<span class='cell-name'>$appName</span><br/>&lt;$companyName&gt;
					</td>
					<td>$countries</td>
					$td_tag$keywords</td>
					<td>
						<div class='options-row'>
							<div><input id='options-input' name='hourLim_$id' type='text' value='$hourLimit'/></div>
							<div><input id='options-input' name='dayLim_$id' type='text' value='$dayLimit'/></div>
							<div><input id='options-input' name='monthLim_$id' type='text' value='$monthLimit'/></div>
						</div>
					</td>
					<td class='text-center'><strong>$todayInstalls</strong></td>
					<td class='text-center'><strong>$price</strong></td>
					<td class='text-center'><strong>$totalInstalls</strong></td>
					<td class='text-center'><input type='checkbox' $hiddenText name='hidden_$id'/></td>
				</tr>";
}

?>
