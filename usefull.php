<?php

	error_reporting(E_ALL ^ E_NOTICE);
	
	function debug_to_console( $data ) {

			if ( is_array( $data ) )
				$output = "<script>console.log( 'Debug Objects: " . implode( ',', $data) . "' );</script>";
			else
				$output = "<script>console.log( 'Debug Objects: " . $data . "' );</script>";

			echo $output;
	}
	
	function isValid($str) {
			$valid = true;
			$arr = str_split($str);
			for ($i = 0;$i < count($arr);$i++){
				if (!preg_match('/[A-Za-z0-9_-]/', $arr[$i]))
					return false;
			}
			return true;
		}

	function generate_session($salt){
		$randomString = generateRandomString(20);
		$final_string = md5($salt.$randomString);
		return $final_string;
	}

	function generateRandomString($length) {
		$characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
		$charactersLength = strlen($characters);
		$randomString = '';
		for ($i = 0; $i < $length; $i++) {
			$randomString .= $characters[rand(0, $charactersLength - 1)];
		}
		return $randomString;
	}
	
	function localRedirect($localpage){
		echo "<script type=\"text/javascript\">
								   window.location = \"${localpage}\"
							  </script>";
	}
	
	function redirect_post($url, $data, $headers = null) {
		$params = array(
			'http' => array(
				'method' => 'POST',
				'content' => http_build_query($data)
			)
		);
		if (!is_null($headers)) {
			$params['http']['header'] = '';
			foreach ($headers as $k => $v) {
				$params['http']['header'] .= "$k: $v\n";
			}
		}
		$ctx = stream_context_create($params);
		$fp = @fopen($url, 'rb', false, $ctx);
		if ($fp) {
			echo @stream_get_contents($fp);
			die();
		} else {
			// Error
			throw new Exception("Error loading '$url', $php_errormsg");
		}
	}

?>